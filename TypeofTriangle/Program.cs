﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeofTriangle
{
    /// <summary>
    /// This is a console application which accepts three numbers(sides) of a triangle and displays result depends upon the input 
    /// </summary>
    public enum TriangleType
    {
        Scalene = 1, // no two sides are the same length
        Isosceles = 2, // two sides are the same length and one differs
        Equilateral = 3, // all sides are the same length
        Error = 4 // inputs can't produce side1 triangle
    }
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number for side 1 ");
            var side1=  Console.ReadLine();
            Console.WriteLine("Enter number for side 2 ");
            var side2 = Console.ReadLine();
            Console.WriteLine("Enter number for side 3");
            var side3 = Console.ReadLine();
            var ttype = GetTriangleType(Convert.ToInt32(side1), Convert.ToInt32(side2), Convert.ToInt32(side3));
            Console.WriteLine("Type of triangle is " + ttype);
            Console.ReadLine();

        }
        public static TriangleType GetTriangleType(int side1, int side2, int side3)
        {
            var values = new int[3] { side1, side2, side3 };


            if (side1 > 0 && side2 > 0 && side3 > 0)
                switch (values.Distinct().Count())
                {
                    case 1:
                        return TriangleType.Equilateral;
                    case 2:
                        return TriangleType.Isosceles;
                    case 3:
                        return TriangleType.Scalene;
                    default:
                        return TriangleType.Error;
                }
            else
                return TriangleType.Error;
        }
    }
}
