# README #

This is Type of triangle solution is a console application which accepts 3 sides and displays the type of triangle.

### TypeofTriangle ###

This repository is to display the type of triangle for given sides. 

### Setup Instruction ###

There are two project in this solution. 
1. TypeofTriangle -> This project contains source code. Download,build and run the solution. 
2. TestTypeofTriangle -> This is test project. 