﻿using System;
using System.Linq;
using NUnit.Framework;
using TypeofTriangle; 

namespace TestTypeofTriangle
{
       /// <summary>
       /// This is unit test project to test all the possible scenarios including the failure 
       /// </summary>

      

        [TestFixture]
        public class TriangleTesterTests
        {
            [Test]
            public void Test_GetTriangleType()
            {
                
            NUnit.Framework.Assert.AreEqual(TriangleType.Equilateral,
                TypeofTriangle.Program.GetTriangleType(4, 4, 4),
                                "GetTriangleType(4, 4, 4) did not return Equilateral");

                NUnit.Framework.Assert.AreEqual(TriangleType.Isosceles,
                    TypeofTriangle.Program.GetTriangleType(4, 4, 3),
                                "GetTriangleType(4, 4, 3) did not return Isosceles");

                NUnit.Framework.Assert.AreEqual(TriangleType.Scalene,
                    TypeofTriangle.Program.GetTriangleType(4, 3, 2),
                                "GetTriangleType(4, 3, 2) did not return Scalene");

                NUnit.Framework.Assert.AreEqual(TriangleType.Error,
                    TypeofTriangle.Program.GetTriangleType(-4, 4, 4),
                                "GetTriangleType(-4, 4, 4) did not return Error");

                NUnit.Framework.Assert.AreEqual(TriangleType.Error,
                    TypeofTriangle.Program.GetTriangleType(4, -4, 4),
                                "GetTriangleType(4, -4, 4) did not return Error");

                NUnit.Framework.Assert.AreEqual(TriangleType.Error,
                    TypeofTriangle.Program.GetTriangleType(4, 4, -4),
                                "GetTriangleType(4, 4, -4) did not return Error");
            }
        }

    
}
